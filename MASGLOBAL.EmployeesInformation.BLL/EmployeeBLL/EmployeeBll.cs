﻿namespace MASGLOBAL.EmployeesInformation.BLL.EmployeeBLL
{
    using MASGLOBAL.EmployeesInformation.BLL.EmployeeFactory;
    using MASGLOBAL.EmployeesInformation.Common;
    using MASGLOBAL.EmployeesInformation.DAL.Employee;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class EmployeeBll : IEmployeeBll
    {
        private IEmployeeRepository _objEmployeeRepository;

        public EmployeeBll(IEmployeeRepository objEmployeeRepository)
        {
            this._objEmployeeRepository = objEmployeeRepository;
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            try
            {
                var employees = await _objEmployeeRepository.GetAll();
                List<Employee> lstEmployee = new List<Employee>();
                //Loop every employee to instantiate with factory and calculate anual salary
                foreach (var emp in employees)
                {
                    Employee employee = EmployeeFactory.GetEmployee(emp);
                    lstEmployee.Add(employee);
                }
                return lstEmployee;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<Employee> GetById(int id)
        {
            try
            {
                var emp = await _objEmployeeRepository.GetById(id);
                if (emp != null)
                {
                    return EmployeeFactory.GetEmployee(emp);
                }
                return null;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
