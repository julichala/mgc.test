﻿namespace MASGLOBAL.EmployeesInformation.BLL.EmployeeBLL
{
    using Common;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public interface IEmployeeBll
    {
        Task<IEnumerable<Employee>> GetAll();
        Task<Employee> GetById(int id);
    }
}
