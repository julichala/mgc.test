﻿namespace MASGLOBAL.EmployeesInformation.Tests.Employee
{
    using MASGLOBAL.EmployeesInformation.BLL.EmployeeBLL;
    using MASGLOBAL.EmployeesInformation.DAL.Employee;
    using MASGLOBAL.EmployeesInformation.DAL.Employee.Fakes;
    using MASGLOBAL.EmployeesInformation.DAL.Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [TestClass]
    public class EmployeeTest
    {
        [TestMethod]
        public async Task TestYearSalary()
        {
            IEnumerable<EmployeeModel> lstEmployee = new List<EmployeeModel>()
            {
                new EmployeeModel()
                {

                    Id = 3,
                    name = "Julian",
                    roleId = 1,
                    contractTypeName = "HourlySalaryEmployee",
                    hourlySalary = 8000

                },
                new EmployeeModel()
                {

                    Id = 5,
                    name = "Andres",
                    roleId = 1,
                    contractTypeName = "MonthlySalaryEmployee",
                    monthlySalary = 200000

                }
            };
            

            IEmployeeRepository objEmployeeRepository = new StubIEmployeeRepository()
            {
                GetAll = () => Task.FromResult(lstEmployee)
            };

            //Arrange
            decimal expectedAnualSalary1 = 11520000;
            decimal expectedAnualSalary2 = 2400000;

            //Act

            EmployeeBll objEmployeeBll = new EmployeeBll(objEmployeeRepository);
            
            var employees = await objEmployeeBll.GetAll();

            List<Common.Employee> lstEmployees = employees.ToList();

            //Assert
            Assert.AreEqual(lstEmployees.ElementAt(0).yearSalary, expectedAnualSalary1);
            Assert.AreEqual(lstEmployees.ElementAt(1).yearSalary, expectedAnualSalary2);
        }
    }
}