﻿namespace MASGLOBAL.EmployeesInformation.DAL.Employee
{
    using MASGLOBAL.EmployeesInformation.DAL.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IEmployeeRepository
    {
        Task<IEnumerable<EmployeeModel>> GetAll();
        Task<EmployeeModel> GetById(int id);
    }
}
