﻿namespace MASGLOBAL.EmployeesInformation.DAL.Models
{
    using System;
    public class EmployeeModel
    {
        public Int32 Id { get; set; }
        public string name { get; set; }
        public string contractTypeName { get; set; }
        public int roleId { get; set; }
        public string roleName { get; set; }
        public string roleDescription { get; set; }
        public decimal hourlySalary { get; set; }
        public decimal monthlySalary { get; set; }
    }
}
