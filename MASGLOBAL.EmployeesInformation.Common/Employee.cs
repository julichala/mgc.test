﻿namespace MASGLOBAL.EmployeesInformation.Common
{
    using System;

    public abstract class Employee
    {
        public Int32 Id { get; set; }
        public string name { get; set; }
        public string contractTypeName { get; set; }
        public int roleId { get; set; }
        public string roleName { get; set; }
        public string roleDescription { get; set; }
        public virtual decimal yearSalary { get; }
    }
}
