﻿namespace MASGLOBAL.EmployeesInformation.Common.Entities
{
    public class HourlyEmployee : Employee
    {
        public decimal hourlySalary { get; set; }
        public override decimal yearSalary => this.hourlySalary * 120 * 12;
    }
}