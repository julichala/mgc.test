﻿namespace MASGLOBAL.EmployeesInformation.Common.Entities
{
    public class MonthlyEmployee : Employee
    {
        public decimal monthlySalary { get; set; }
        public override decimal yearSalary => this.monthlySalary * 12;
    }
}
